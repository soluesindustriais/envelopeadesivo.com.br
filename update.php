<?php

ini_set('memory_limit', -1);
ini_set("max_execution_time", 260 * 60);

if (isset($_GET['token'])) {

    $token = trim(filter_input(INPUT_GET, 'token'));

    $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR;
    foreach (glob($dir . "site_*zip") as $file) {
        $zip = new ZipArchive();

        if (file_exists($file)) {
            if ($zip->open($file)) {
                error_log("Arquivo " . $file);
                $total = $zip->numFiles;                
                error_log("Total de arquivos " . $total);
                error_log("------------------------------------ ");
                for ($i = 0; $i <= $total; $i++) {
                    $arquivo = $zip->statIndex($i);
                    $final = $dir . $arquivo['name'];

                    if (file_exists($final) && !is_dir($final)) {
                        @unlink($final);
                    }

                    try {
                        $zip->extractTo($dir, array($arquivo['name']));
                    } catch (Exception $exc) {
                        error_log("erros_desconpactar " . $exc->getMessage());
                    }
                }
                $zip->close();
            }
            if (file_exists($file)) {
                unlink($file);
            }
        }
        unset($zip);
    }
}