var CotacaoGa = {
    eventoCategoria: "Cotação",
    eventoArea: "",
    eventoLocal: "",
    eventoValor: 0,
    enviarCotacao: function () {
        if (typeof ga != "undefined") {
            ga('send', 'event', {
                eventCategory: CotacaoGa.eventoCategoria,
                eventAction: CotacaoGa.eventoArea,
                eventLabel: CotacaoGa.eventoLocal,
                eventValue: CotacaoGa.eventoValor,
            });
        }
    },
    enviarIntencaoPagina: function (pagina) {
        CotacaoGa.eventoArea = "Pagina";
        CotacaoGa.eventoLocal = "Intencao " + pagina;
        CotacaoGa.eventoValor = 0;
        CotacaoGa.enviarCotacao();
    },
    enviarSucessoPagina: function (pagina) {
        CotacaoGa.eventoArea = "Pagina";
        CotacaoGa.eventoLocal = "Sucesso " + pagina;
        CotacaoGa.eventoValor = 1;
        CotacaoGa.enviarCotacao();
    },
    enviarIntencaoImagem: function (imagem) {
        CotacaoGa.eventoArea = "Imagem";
        CotacaoGa.eventoLocal = "Intencao " + imagem;
        CotacaoGa.eventoValor = 0;
        CotacaoGa.enviarCotacao();
    },
    enviarSucessoImagem: function (imagem) {
        CotacaoGa.eventoArea = "Imagem";
        CotacaoGa.eventoLocal = "Intencao " + imagem;
        CotacaoGa.eventoValor = 1;
        CotacaoGa.enviarCotacao();
    },
}

var selecionarIntencaoArea = function (objetoElemento) {
    var texto = "";
    var area = $(objetoElemento).data('area');

    switch (area) {
        case 'pagina':
            texto = document.location.pathname;
            CotacaoGa.enviarIntencaoPagina(texto);
            break;
        case 'imagem':
            texto = $("input[name=produto_nome]").val();
            CotacaoGa.enviarIntencaoImagem(texto);
            break;

    }
}
var selecionarSucessoArea = function () {

    var texto = "";
    var produto = $("input[name=produto_nome]").val();

    if (produto != "") {
        texto = $("input[name=produto_nome]").val();
        CotacaoGa.enviarSucessoImagem(texto);
    } else {
        texto = document.location.pathname;
        CotacaoGa.enviarSucessoPagina(texto);
    }
}

$(document).ready(function () {
    var elemento = null;
    mascaras();
    $(".cssmenu").accordion({
        accordion: false,
        speed: 500
    });
    $("#servicosTabs").organicTabs();

    $("#servicosTabsDois").organicTabs();

    $("#novoNumero").click(function () {
        trocarCaptcha();
    });

    $(this).on("click", ".btn-cotacao", function (e) {
        e.preventDefault();
        var palavra = $(".fancybox-outer h2").html();
        var nome_produto = $(".fancybox-outer h3").html();
        var referencia = $(".lightbox-fixed-ref").html();
        var imagem = $(".fancybox-image").attr("src");

        $("#form-cotacao").removeClass('hidden');
        $("#box-cotacao-enviada").addClass('hidden');

        if ($(".fancybox-inner:visible").length >= 1) {
            $.fancybox.close();
        }
        $('html, body').animate({
            scrollTop: $("#faca-sua-cotacao").offset().top
        }, 500);

        $(".error-message").html("");
        $("#sucesso_mensagem").html("");
//                console.log(this)
//                var palavra = this.title;
        $("textarea[name=mensagem]").val("");
        this.title = "Cotação do produto " + palavra;

        $("#title-empresa").html("<h2>" + palavra + "</h2>");
        $("#title-keyword").html("<h3>Cotando o produto: " + palavra + "</h3>");
        $("#fancyalter").attr({"alt": palavra, "title": palavra});

        $("#produto_nome").html(nome_produto);
        $("input[name=imagem]").val(imagem);
        $("input[name=produto_nome]").val(nome_produto);
        $("input[name=produto_url]").val(window.location.href);
        $("input[name=produto_ref]").val(referencia);
//                mascaras();

        selecionarIntencaoArea(this);

    });

    $(".follow-click").click(function (e) {
//        e.preventDefault()

        var clickOrigem = $(this).data("origem")
        var clickConteudo = $(this).html()
        elemento = $.ajax({
            url: document.URL,
            dataType: "text",
            type: "POST",
            data: {acao: "click", origem: clickOrigem, conteudo: clickConteudo, url: document.URL, pagina: $("body").data("pagina"), categoria: $("body").data("categoria")},
            beforeSend: function () {
            },
            complete: function () {
                elemento = null;
            }
        });

    });
    $(this).on('click', "#btn-enviar-cotacao", function (e) {
        e.preventDefault();

        $.ajax({
            url: $("#formulario-cotacao").attr("action"),
            dataType: "json",
            type: "POST",
            data: $("#formulario-cotacao").serialize(),
            beforeSend: function () {
                $(".error-message").html("");
                $("#sucesso_mensagem").html("");
                $("#loader-ajax").show();
                $("#btn-enviar-cotacao").hide();
            },
            success: function (data) {
                trocarCaptcha();
                $("#btn-enviar-cotacao").show();
                $("#loader-ajax").hide();

                if (typeof data.error !== "undefined") {
                    for (var i in data.error) {
                        if ($("#erro_" + i).length > 0) {
                            $("#erro_" + i).html(data.error[i]);
                        }
                    }

                } else if (typeof data.resultado !== "undefined") {
                    if (data.resultado == "") {
                        $("#form-cotacao").addClass("hidden");
                        $("#box-cotacao-enviada").removeClass("hidden");
                        selecionarSucessoArea();
                    } else {
                        $("#sucesso_mensagem").html(data.resultado);
                    }
                }
            },
            complete: function () {
                elemento = null;
            }
        });
    });

})

function trocarCaptcha() {
    $("#novoNumero").attr("src", "/captcha.php?" + Date.now());
}

function UcWords(campo) {
    val = campo.value;
    newVal = val.toLowerCase().replace(/[^A-Za-zÃ¡Ã¢Ã Ã£Ã¤Ã©ÃªÃ¨Ã«Ã­Ã®Ã¬Ã¯Ã³ÃµÃ²Ã´Ã¶ÃºÃ¹Ã»Ã¼Ã§][A-Za-zÃ¡Ã¢Ã Ã£Ã¤Ã©ÃªÃ¨Ã«Ã­Ã®Ã¬Ã¯Ã³ÃµÃ²Ã´Ã¶ÃºÃ¹Ã»Ã¼Ã§]/g, function (m) {
        return m.toUpperCase()
    }).replace(/[0-9][A-Za-zÃ¡Ã¢Ã Ã£Ã¤Ã©ÃªÃ¨Ã«Ã­Ã®Ã¬Ã¯Ã³ÃµÃ²Ã´Ã¶ÃºÃ¹Ã»Ã¼Ã§]/g, function (m) {
        return m.toUpperCase()
    }).replace(/( (da|das|e|de|do|dos|para|na|nas|no|nos) )/gi, function (m) {
        return m.toLowerCase()
    }).replace(/^./, function (m) {
        return m.toUpperCase()
    })
    if (val != newVal) {
        campo.value = newVal;
    }
}


function mascaras() {
    $(".telefone").mask("(99) 9999-9999?9");
    $(".telefone-sem-ddd").mask("9999-9999?9");
}

(function(i,s,o,g,r,a,m){
i['GoogleAnalyticsObject']=r;
i[r]=i[r] || function(){(
i[r].q=i[r].q||[]).push(arguments)},
i[r].l=1*new Date();
a=s.createElement(o),
m=s.getElementsByTagName(o)[0];
a.async=1;
a.src=g;
m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-47730935-51', 'auto', 'clientTracker');
ga('send', 'pageview');
ga('clientTracker.send', 'pageview');